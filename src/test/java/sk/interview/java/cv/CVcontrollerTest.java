package sk.interview.java.cv;


import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.contains;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(controllers = CVcontroller.class) ... not working
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CVcontrollerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("testing /simple endpoint")
    public void requestingSimpleEndpoint() throws Exception {
        mvc.perform(get("/api/simple"))
                .andExpect(content().string(containsString("Rundown")));
    }

    @Test
    //@Disabled("endpoint not working")
    public void requestingAll_shouldReturnStatusOK() throws Exception {
        mvc.perform(get("/api/cv"))
            .andExpect(status().isOk());
    }

}
