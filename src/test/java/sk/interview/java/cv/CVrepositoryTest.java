package sk.interview.java.cv;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import sk.interview.java.cv.CVmodel;

import static org.assertj.core.api.Assertions.assertThat;

// @DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = CVrepository.class))
// @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class CVrepositoryTest {

    @Autowired
    private CVrepository repository;

    @Autowired
    private TestEntityManager entityManager;

    private CVmodel cvmodel;

    @BeforeEach
    void setup() {

        cvmodel = new CVmodel();
        cvmodel.setFirstName("Feri");
        cvmodel.setLastName("Kovac");
        cvmodel.setProjectName("test project");
    }

    @Test
    public void addNewProjectToDatabaseTest() throws Exception {

        CVmodel mymodel = new CVmodel();

        mymodel.setFirstName("Frank");
        mymodel.setLastName("Smith");
        mymodel.setProjectName("assertj test");

            assertThat(mymodel.getFirstName()).contains("Frank");

        assertThat(entityManager).isNotNull();
    }


}
