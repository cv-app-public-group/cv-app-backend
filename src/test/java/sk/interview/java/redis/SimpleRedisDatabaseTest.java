package sk.interview.java.redis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleRedisDatabaseTest {

    @Autowired
    RedisRepository repository;

    @Test
    public void storeProjectObjectInRedis() {

        RedisModel newProject = new RedisModel("1","Feri", "Kovac", "redis project");

        repository.save(newProject);

        List<RedisModel> projects = (List<RedisModel>) repository.findAll();

        projects.stream().forEach( project -> {
            assertThat(project.getProjectName()).contains("redis");
        });

        repository.findAll()
                .forEach( project -> {
                    assertThat(project.getProjectName()).isEqualTo("redis project");
                });
    }


}
