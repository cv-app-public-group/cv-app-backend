package sk.interview.java.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


/*
    1. create DataSourceProperties objects that pull connection string from application.properties
    2. create DataSource objects off of the DataSourceProperties objects
 */

/*
@Configuration
public class MultipleDataSourcesConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.mysql")
    public DataSourceProperties getMysqlProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.data.mongodb")
    public DataSourceProperties getMongodbProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public DataSource createMysqlSource() {
        return getMysqlProperties()
                .initializeDataSourceBuilder()
                .build();
    }

    @Bean
    public DataSource createMongodbSource() {
        return getMongodbProperties()
                .initializeDataSourceBuilder()
                .build();
    }

}
 */

