package sk.interview.java.cv;

// https://www.callicoder.com/spring-boot-rest-api-tutorial-with-mysql-jpa-hibernate/

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

// domain model/persistence/entity class

@Entity
@Table(name = "cvprojects", schema = "interviewDB")
@EntityListeners(AuditingEntityListener.class)      // enabling automatic population of timestamps
@JsonIgnoreProperties(
        value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class CVmodel implements Serializable {

    @Id
    @GeneratedValue
    // @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "first_name")
    @NotBlank
    private String firstName;

    @Column(name = "last_name")
    @NotBlank
    private String lastName;

    @Column(name = "project_name")
    @NotBlank
    private String projectName;

    @Column(name = "created_at", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(name = "updated_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;


    public CVmodel() {}

    public CVmodel(String firstName, String lastName, String projectName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.projectName = projectName;
    }


    public Long getId() {
        return id;
    }
    
    public void setFirstName(String value) {
        firstName = value;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String value) {
        lastName = value;
    }

    public String getLastName() {
        return lastName;
    }

    public void setProjectName(String value) {
        projectName = value;
    }

    public String getProjectName() {
        return projectName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }


    @Override
    public String toString() {
        return "CVmodel [id=" + id + ", firstName=" + firstName + ", lastName=" +
                lastName + ", projectName=" + projectName + "]";
    }



}
