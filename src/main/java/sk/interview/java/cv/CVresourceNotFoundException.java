package sk.interview.java.cv;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CVresourceNotFoundException extends RuntimeException {

    private String fieldName;
    private Object fieldValue;

    public CVresourceNotFoundException(String fieldName, Object fieldValue) {

        super(String.format("CV project not found for %s = %s", fieldName, fieldValue));

        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public String getFieldName() { return fieldName; }

    public Object getFieldValue() { return fieldValue; }
}
