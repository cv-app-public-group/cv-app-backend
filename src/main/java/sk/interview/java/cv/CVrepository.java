package sk.interview.java.cv;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CVrepository extends JpaRepository<CVmodel, Long>{

}