package sk.interview.java.cv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.*;
import sk.interview.java.generic.ResourceNotFoundException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class CVcontroller {

    @Autowired
    CVrepository repository;

    @GetMapping("/simple")
    public String displayText() {
        return "Rundown of CV projects for an interview.";
    }

    @GetMapping("/cv")
    public List<CVmodel> getAllCVprojects() {
        return repository.findAll();
    }

    @PostMapping("/cv")
    public CVmodel createCVproject(@Valid @RequestBody CVmodel data) {
        // @Valid - refers to @NotBlank fields in entity
        //        - if not, then 400 BadRequest returned
        return repository.save(data);
    }

    @GetMapping("/cv/{id}")
    public CVmodel getCVprojectById(@PathVariable Long id) {
        return repository.findById(id)
                   .orElseThrow( () -> new CVresourceNotFoundException("ID", id) );
    }

    @PutMapping("/cv/{id}")
    public CVmodel updateCVproject(@PathVariable Long id,
                                   @Valid @RequestBody CVmodel newData) {

        Optional<CVmodel> cv = repository.findById(id);
                 //.orElseThrow( () -> new ResourceNotFoundException() );


        if (cv.isPresent()) {
            CVmodel item = cv.get();

            item.setFirstName(newData.getFirstName());
            item.setLastName(newData.getLastName());
            item.setProjectName(newData.getProjectName());

            return repository.save(item);
        } else {
            return new CVmodel();
        }
    }

    @DeleteMapping("/cv/{id}")
    public ResponseEntity<?> deleteCVproject(@PathVariable Long id) {

        Optional<CVmodel> cv = repository.findById(id);
        // .orElseThrow( () -> new CustomException() );

        if(cv.isPresent()) {
            repository.delete(cv.get());

            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }



    }

}
