package sk.interview.java.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sk.interview.java.generic.ResourceNotFoundException;

//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    RedisRepository repository;

    @GetMapping("/projects")
    public Iterable<RedisModel> getAllProjects() {

        return repository.findAll();
    }

    @PostMapping("/projects/save")
    public RedisModel saveProject(@RequestBody RedisModel data) {

        return repository.save(data);
    }

    @GetMapping("/projects/{id}")
    public ResponseEntity<RedisModel> getProject(@PathVariable Long id)
        throws ResourceNotFoundException {

        RedisModel model = repository.findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("Project not found for id = " + id) );

        //return ResponseEntity.ok().body(model);
        return ResponseEntity.ok().body(model);
    }

    @PutMapping("/projects/{id}")
    public RedisModel updateProject(@PathVariable Long id, @RequestBody RedisModel data)
        throws ResourceNotFoundException {

        RedisModel model = repository.findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("Project not found for id = " + id) );

        model.setFirstName(data.getFirstName());
        model.setLastName((data.getLastName()));
        model.setProjectName(data.getProjectName());

        return repository.save(model);
    }

    @DeleteMapping("/projects/{id}")
    public ResponseEntity<?> deleteProject(@PathVariable Long id)
        throws ResourceNotFoundException {

        RedisModel model = repository.findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("Project not found for id = " + id) );

        repository.delete(model);

        return ResponseEntity.ok().build();
    }


}
