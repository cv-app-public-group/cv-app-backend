package sk.interview.java.redis;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash("Project")
public class RedisModel implements Serializable {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String projectName;

    public RedisModel() {}

    public RedisModel(String id, String firstName, String lastName, String projectName) {

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.projectName = projectName;
    }


    public void setId(String value) {
        id = value;
    }

    public String getId() {
        return id;
    }

    public void setFirstName(String value) {
        firstName = value;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String value) {
        lastName = value;
    }

    public String getLastName() {
        return lastName;
    }

    public void setProjectName(String value) {
        projectName = value;
    }

    public String getProjectName() {
        return projectName;
    }

    @Override
    public String toString() {
        return "Domain model for Redis: Project [id=" + id + ", firstName=" + firstName +
               ", lastName=" + lastName + ", projectName=" + projectName + "]";
    }
}
