package sk.interview.java.kafkatest;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {

    private static final Logger logger =
            LoggerFactory.getLogger(ConsumerService.class);


    @KafkaListener(topics = "${spring.kafka.topic1}",
                   groupId = "${spring.kafka.consumer.group-id}",
                   containerFactory = "jsonListener")
    public void consumeMessage(@Payload BasicMessage message,
                               @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {

        logger.info(String.format("Message received: %s from partition: %d", message.getText(), partition));

    }



}
