package sk.interview.java.kafkatest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class ProducerService {

    private static final Logger logger =
            LoggerFactory.getLogger(ProducerService.class);

    @Autowired
    private KafkaTemplate<String, BasicMessage> kafkaTemplate;

    @Value("${spring.kafka.topic1}")
    private String topicName;


    public void publishMessage(BasicMessage message) {

        logger.info(String.format("Message sent: %s", message.getText()));


        /*
        kafkaTemplate.send(topicName, message);

        System.out.println(String.format("Message sent: %s", message.getText()));
         */


        // ..... handling ListenableFuture object .....
        ListenableFuture<SendResult<String, BasicMessage>> future =
                kafkaTemplate.send(topicName, message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, BasicMessage>>() {

            @Override
            public void onSuccess(SendResult<String, BasicMessage> result) {
                System.out.println("Message sent: (" + message +
                        ") with offset = " + result.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(Throwable ex) {
                System.err.println("Unable to send message (" + message + "" +
                        ") due to " + ex.getMessage());
            }
        });

    }

}
