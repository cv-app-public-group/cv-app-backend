package sk.interview.java.kafkatest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BasicMessage {

    private String text;

    public BasicMessage() {}

    public BasicMessage(String text) {
        this.text = text;
    }


    public void setText(String input) {
        text = input;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "BasicMessage [text = " + text + "]";
    }

}
