package sk.interview.java.kafkatest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/kafka")
public class KafkaController {

    private final ProducerService producerService;
    private final ConsumerService consumerService;


    public KafkaController(
            @Autowired ProducerService producerService,
            @Autowired ConsumerService consumerService) {
        this.producerService = producerService;
        this.consumerService = consumerService;
    }


    @PostMapping(value = "/publish", consumes = "application/json")
    public void publish(@RequestBody BasicMessage message)  {

        producerService.publishMessage(message);
    }




}
