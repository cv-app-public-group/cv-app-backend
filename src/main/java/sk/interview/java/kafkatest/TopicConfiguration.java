package sk.interview.java.kafkatest;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

public class TopicConfiguration {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapHost;

    @Value("${spring.kafka.topic1}")
    private String topic1name;

    @Value("${spring.kafka.topic2}")
    private String topic2name;


    @Bean
    public KafkaAdmin kafkaAdmin() {

        Map<String, Object> props = new HashMap<>();
            props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG,
                    bootstrapHost);

            return new KafkaAdmin(props);
    }

    @Bean
    public NewTopic topic1() {
        return new NewTopic(topic1name, 1, (short) 1);
    }

    @Bean
    public NewTopic topic2() {
        return TopicBuilder
                .name(topic2name)
                .partitions(1)
                .replicas(1)
                .build();
    }


}
