package sk.interview.java.cvprojects;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface CustomProjectRepo {

    List<ProjectModel> lookupByMultipleCriteria(Map<String, String> inputs);

}
