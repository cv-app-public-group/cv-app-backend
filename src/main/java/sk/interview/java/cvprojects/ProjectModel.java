package sk.interview.java.cvprojects;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Document(collection = "cvprojects")
public class ProjectModel {

    @Id
    private String id;

    @NotBlank
    private String firstName;

    @NotBlank
    @Indexed(unique = true)
    private String lastName;

    @NotBlank
    private String projectName;

    public ProjectModel() {}

    public ProjectModel(String firstName, String lastName, String projectName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.projectName = projectName;
    }


    public String getId() {
        return id;
    }

    public void setFirstName(String value) {
        firstName = value;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String value) {
        lastName = value;
    }

    public String getLastName() {
        return lastName;
    }

    public void setProjectName(String value) {
        projectName = value;
    }

    public String getProjectName() {
        return projectName;
    }

    @Override
    public String toString() {
        return "Project [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName +
                ", projectName=" + projectName + "]";
    }

}
