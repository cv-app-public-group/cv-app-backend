package sk.interview.java.cvprojects;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends MongoRepository<ProjectModel, String> {

    List<ProjectModel> findByProjectNameContaining (String value);
    List<ProjectModel> findByProjectNameStartingWith (String value);
    List<ProjectModel> findByProjectNameEndingWith (String value);

    @Query("{ 'projectName' : { $regex : ?0 } }")
    List<ProjectModel> lookupByProjectNameRegex (String name);


}
