package sk.interview.java.cvprojects;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class CustomProjectRepoImpl implements CustomProjectRepo {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<ProjectModel> lookupByMultipleCriteria(Map<String, String> inputs) {

        Query query = new Query();

        query.addCriteria(Criteria.where("projectName").regex(inputs.get("projectName"), "i"));

        return mongoTemplate.find(query, ProjectModel.class);
    }


}
