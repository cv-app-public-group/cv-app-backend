package sk.interview.java.cvprojects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sk.interview.java.generic.ResourceNotFoundException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class ProjectController {

    @Autowired
    private ProjectRepository repository;

    @Autowired
    private CustomProjectRepo customRepository;

    @GetMapping("/cvprojects")
    public List<ProjectModel> getAllOrFilteredProjects (@RequestParam(required = false) Map<String, String> queryParams) {

        List<ProjectModel> projects = repository.findAll();

        if(Objects.nonNull(queryParams)) {

            for ( Map.Entry<String, String> entry : queryParams.entrySet() ) {
                if(entry.getKey().equals("projectName")) {
                    projects = repository.lookupByProjectNameRegex(entry.getValue());
                } else if(entry.getKey().equals("firstName")) {

                } else if(entry.getKey().equals("lastName")) {

                }
            }
        }

        return projects;
    }

    @GetMapping("/cvprojects/criteria")
    List<ProjectModel> getProjectsBasedOnMultipleCriteria(@RequestParam(required = false) Map<String, String> queryParams) {

        return customRepository.lookupByMultipleCriteria(queryParams);
    }

    @PostMapping("/cvprojects")
    public ProjectModel createProject(@Valid @RequestBody ProjectModel data) {
        // @Valid - refers to @NotBlank fields in entity
        //        - if not, then 400 BadRequest returned

        //data.setId(sequenceGenerator.generateSequence(ProjectModel.SEQUENCE_NAME));

        return repository.save(data);
    }

    @GetMapping("/cvprojects/{id}")
    public ResponseEntity<ProjectModel> getProjectById(@PathVariable(value = "id") String id)
            throws ResourceNotFoundException {

        ProjectModel project = repository.findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("Project not found for id = " + id) );

        return ResponseEntity.ok().body(project);
    }

    @PutMapping("/cvprojects/{id}")
    public ResponseEntity<?> updateProject(@PathVariable String id,
                                                      @Valid @RequestBody ProjectModel newData)
            throws ResourceNotFoundException {

        ProjectModel project = repository.findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("Project not found for id = " + id) );

        project.setFirstName(newData.getFirstName());
        project.setLastName(newData.getLastName());
        project.setProjectName(newData.getProjectName());

        ProjectModel updated = repository.save(project);

        // return ResponseEntity.ok(updated);

        String html =
                "<!DOCTYPE html>                        " +
                "<html>                                 " +
                "  <head>                               " +
                "    <title>Error</title>               " +
                "  </head>                              " +
                "  <body>                               " +
                "    <h2>Project not found.</h2> " +
                "  </body>                              " +
                "</html>                                ";

        return ResponseEntity.ok().body(html);
    }

    @DeleteMapping("/cvprojects/{id}")
    public ResponseEntity<?> deleteProject(@PathVariable String id)
        throws ResourceNotFoundException {

        ProjectModel project = repository.findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("Project not found for id = " + id) );

        repository.delete(project);

        return ResponseEntity.ok().build();
    }




}
