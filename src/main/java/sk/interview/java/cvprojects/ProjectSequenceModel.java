package sk.interview.java.cvprojects;

import org.springframework.data.mongodb.core.mapping.Document;
import javax.persistence.Id;


@Document(collection = "cvproject_sequences")
public class ProjectSequenceModel {

    @Id
    private String id;

    private long seq;

    public void setId(String value) {
        id = value;
    }

    public String getId() {
        return id;
    }

    public void setSeq(long value) {
        seq = value;
    }

    public long getSeq() {
        return seq;
    }

}
