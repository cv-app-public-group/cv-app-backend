package sk.interview.java.generic;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


@Controller
public class AppErrorResponse implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {

    /*
         * OPTIONS:
         * javax.servlet.error.status_code
         * javax.servlet.error.message
         * javax.servlet.error.request_uri
         * javax.servlet.error.exception
         * javax.servlet.error.exception_type
         * javax.servlet.error.servlet_name
    */


        int statusCode = (int) request.getAttribute("javax.servlet.error.status_code");
        Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        Object message = request.getAttribute("javax.servlet.error.message");

        return String.format(
                "<!DOCTYPE html>                        " +
                "<html>                                 " +
                "  <head>                               " +
                "    <title>Error</title>               " +
                "  </head>                              " +
                "  <body>                               " +
                "    <h2>Something went wrong.</h2> " +
                "    <div>Status code: %s</div>         " +
                "    <div>Description: %s</div>         " +
                "  </body>                              " +
                "</html>                                ",
                statusCode, message);
    }

    public String getErrorPath() {
        return "/error";
    }

}








