package sk.interview.java.generic;

import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/*  ---- no effect
@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
public class ExceptionController extends AbstractErrorController {

    public ExceptionController(ErrorAttributes attrs) {
        super(attrs);
    }

    @Deprecated
    public String getErrorPath() {
        return null;
    }

    @RequestMapping(produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView errorHtml (HttpServletRequest request, HttpServletResponse response) {
        throw new RuntimeException(getErrorMessage(request));
    }

    @RequestMapping
    public ResponseEntity<Map<String, Object>> error (HttpServletRequest request) {
        throw new RuntimeException(getErrorMessage(request));
    }

    private String getErrorMessage(HttpServletRequest request) {

        Object message = request.getAttribute("javax.servlet.error.message");

        return String.format("Message: %s", message);

    }



}


 */