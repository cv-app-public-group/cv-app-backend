//package sk.interview.java.generic;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//

//  this should resolve dynamic routing when bundling react.js with spring boot

//@Controller
//public class ViewController {
//
//    @RequestMapping({ "/api/{id:\\w+}", "/tests/**" })
//    public String index() {
//        return "forward:/index.html";
//    }
//
//
//}
